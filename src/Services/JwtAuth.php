<?php

namespace App\Services;

use Firebase\JWT\JWT;
use App\Entity\User;


class  JwtAuth{
    
    public $manager;
    public $key;

    public function __construct($manager){
        $this->manager=$manager;
        $this->key='key01234';
    }

    public function signup($email, $password, $gettoken=null){
        //  check if the user exists
            $user_repo=$this->manager->GetRepository(User::class);
            
            $user=$user_repo->findOneBy([
                'email'=>$email,
                'password'=>$password
            ]);
            
        // default answer

            $data=[
                    'status'=>'error',
                    'code'=>'200',
                    'msg'=>'user not found',
            ];

        //  if the user exists generate the token

            $signup=false;
            if(is_object($user)){
                $signup=true;
            }

            if($signup){

                $token=[
                        'sub'=>$user->getId(),
                        'name'=>$user->getName(),
                        'surname'=>$user->getSurname(),
                        'description'=>$user->getDescription(),
                        'avatar'=>$user->getAvatar(),
                        'role'=>$user->getRole(),
                        'email'=>$user->getEmail(),
                        'iat'=>time(),
                        'exp'=>time() + (7 * 24 *60 *60),

                ];
        //code le token

            $jwt=JWT::encode($token,$this->key,'HS256');

        //  check the flag gettoken with a condition
           
            if(!empty($gettoken)){
                $data=$jwt;
            }else{
                $decode=JWT::decode($jwt,$this->key,['HS256']);
                $data=$decode; 
            }
        }

        //  return the data  

          return $data;
    }
}