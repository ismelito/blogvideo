<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;


use App\Entity\User;
use App\Entity\Video;
use App\Services\JwtAuth;




class UserController extends AbstractController
{
    private function resjson($data /* recoi un parametre avev le donne qui doit serialize*/){

       // 1- Serialize des donnes avec service serialize(convertir objetos en objetos
       // mas simples para poder tenerlos en json )

       $json= $this->get('serializer')->serialize($data,'json');


       // 2- Response avec HttpFoundation
       $response = new Response();

       // 3- Donne un contenu a la reponse
       $response->setContent($json);

       // 4- Indique format de la reponse 
       $response->headers->set('Content-type','application/json');

       // 5 -retourne la reponse
       return $response;
    }
    
    public function index()
    {
        $user_repo=$this->getDoctrine()->getRepository(User::class) ;
        $video_repo=$this->getDoctrine()->getRepository(Video::class);
        $users=$user_repo->findAll();
        $videos=$video_repo->findAll();
        $user=$user_repo->find(1);
        //var_dump($user);
       // die();
        return $this->resjson($videos);

/*$data =[

    'message'=>"controller",
    'path'=>'src\Controller\UserController.php'
];

       foreach($users as $user){

           echo "<h1>{$user->getName()} <br>{$user->getSurname()}  </h1>";
                    
        }

        foreach($user->getVideos() as $videos) {
            echo "<h1>{$videos->getTitle()} - - {$videos->getUser()->getRole()}</h1>";
          
        }       
      die(); // para que no devuelva ningun json

    return $this->json([
    'messahe'=>"controller",
    'path'=>'src\Controller\UserController.php',



]);*/


     /*   return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController'
            
        ]);*/
    }

   public function userCreate (Request $request){


      // take the data by post (recoger) in case it doesn't arrive it will be null
            $json=$request->get('json',null); 

      //decode the json

            // $param= json_decode($json,true); dos formas de hacerlo para descodificar un objeto 
            $param= json_decode($json);// utilizando JsonResponse
     
      //default answer

            $data=[
                'status'=>'error',
                'code'  =>200,
                ' msg'  =>'utilisateur non enregistré',
            //  'json'=>$param,  es pra saber si param descodifico los valores que llegan por post y si no esta vacia 
            ];

      //check and validate data

            if($json!=null){
                   
            
                
                // si es distinta de vacia $name=$param->name si no $name=$param->name=null  
                    $name=(!empty($param->name)) ? $param->name:null;
                    $surname=(!empty($param->surname)) ? $param->surname:null;
                    $password=(!empty($param->password)) ? $param->password:null;
                    $description=(!empty($param->description)) ? $param->description:null;
                    $avatar=(!empty($param->avatar)) ? $param->avatar:null;
                    $role=(!empty($param->role)) ? $param->role:null;
                    $email=(!empty($param->email)) ? $param->email:null;
                    // $validate_email pra comprobar si un email el que llega por post
                    $validator=Validation::createValidator();
                    $validate_email=$validator->validate($email,[ new Email()]);  


                            
                    if( !empty($email) && count($validate_email) == 0 && !empty($password) && !empty($name)
                     && !empty($surname)&& !empty($description) && !empty($avatar) && !empty($role) ){
                        
                        //if the validation is correct create the user object    
                            $user=new User();
                            $user->setName($name);
                            $user->setSurname($surname);

                        // --encrypt the password--
                           $pwd=Hash('sha256',$password);
                           $user->setPassword($pwd);
                           
                        //-- fin encrypt the password --  
                            $user->setDescription($description);
                            $user->setAvatar($avatar);
                            $user->setRole($role);                        
                            $user->setCreateAt(new \DateTime('now'));
                            $user->setEmail($email);
                            
                        //check if the user exists
                            
                            $doctrine=$this->getDoctrine();
                            $em=$doctrine->getManager();
                            $user_repository=$doctrine->getRepository(User::class);
                            $isset_user=$user_repository->findBy(array('email'=> $email));

                            if(count($isset_user)==0){

                                //if the user does not exist save it in the database
                               $em->persist($user);
                               $em->flush();
                                $data=[
                                       'status'=>'success',
                                       'code'=>200,
                                       'msg'=>'usuario logueado',
                                       'user'=>$user,
                                      ];


                            } else{
                                    $data=[
                                        'status'=>'error',
                                        'code'=>400,
                                        'msg'=>'usuario ya existe',
                                        'user'=>$user,
                                    ];

                            }                    

                     }


            }
            

    
     

      


      //make an answer in json
         
         
     // return $this->resjson($data); dos formas de hacerlo para descodificar un objeto 
        return $this->resjson($data); // utilizando new JsonResponse($data)

    }

    public function userLogin(Request $request, JwtAuth $jwtauth){
         
         $json=$request->get('json',null);

         $params=json_decode($json);

         $data=[
                 'status'=>'error',
                 'code'=>'200',
                 'msg'=>'the unregister user',

               ];

        if($json!=null){

            $email=(!empty($params->email)) ? $params->email : null ;
            $password=(!empty($params->password)) ? $params->password :null;
            $gettoken=(!empty($params->gettoken)) ? $params->gettoken :null;

            $validator=Validation::createValidator();
            $validate_email=$validator->validate($email,[new Email ]);

            if(!empty($email) && !empty($password) && count($validate_email)==0){

                $pwd=Hash('sha256',$password);

                if($gettoken){

                    $signup=$jwtauth->signup($email,$pwd,$gettoken);
                }else{
                    $signup=$jwtauth->signup($email,$pwd);
                }
                

            }
            return new JsonResponse($signup);

        }       
        return $this->resjson($data);
    }

   
}
